import "antd/dist/antd.css";
import React, { useState } from "react";
import { UploadOutlined } from "@ant-design/icons";
import { Card, Row, Col, Form, Input,InputNumber, Upload, Checkbox, Button, Table, Space,} from "antd";

let formvalue = [];
let tableValue = [];
let count = 0;
const { Column, ColumnGroup } = Table;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};



const CreateProduct = () => {
  const [form] = Form.useForm();
  const [productAdd, setProductAdd] = useState([]);
  const [dataSource, setDataSource] = useState([]);
  const [isPreviewShown, setPreviewShown] = useState(false);

  const [id, setId] = useState(1);
  const normFile = (e) => {
    return e && e.fileList;
  };

  const handlePreview = () => {
    let val = document.getElementById("show");
    if (val.innerHTML == "Show Data") {
      setPreviewShown(true);
      val.innerHTML = "Hide Data";
    } else {
      val.innerHTML = "Show Data";
      setPreviewShown(false);
    }
  };
  const filterdata = (idp) => {
    console.log(idp);
    return <></>;
  };

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    setId(id + 1);
    values.id = id;
    formvalue = productAdd;
    formvalue.push(values);
    setProductAdd(formvalue);
    console.log(productAdd);

    const data = {};

    data.key = values.id;
    data.ID = values.id;
    data.Name = values.title;
    data.Description = values.description;
    data.Cost = values.MRP;
    data.SellingPrize = values.SP;
    tableValue = dataSource;
    tableValue.push(data);
    setDataSource(tableValue);
    console.log("tablevalue", dataSource);
    alert("Product Created sucessfully...");
  };

  return (
    <div className="site-card-wrapper">
      <Row gutter={[16, 48]}>
        <Col className="title" span={24}>
          Create Product
        </Col>
      </Row>

      {/* -----------Create Form---------------------------------------------------------------------- */}

      <Row gutter={[16, 16]}>
        <Col className="title" span={24}>
          <Card className="site-card-wrapper">
            <Form
              {...formItemLayout}
              form={form}
              name="register"
              onFinish={onFinish}
            >
              <Form.Item
                name="title"
                label="Product Title"
                rules={[
                  {
                    required: true,
                    message: "Please input your Product Title!",
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="description"
                label="Description"
                rules={[
                  {
                    required: true,
                    message: "Please input description of Product",
                  },
                ]}
              >
                <Input.TextArea showCount maxLength={100} />
              </Form.Item>
              <Form.Item
                name="MRP"
                label="MRP"
                rules={[
                  {
                    required: true,
                    message: "Please input MRP of Product",
                  },
                ]}
              >
                <InputNumber />
              </Form.Item>
              <Form.Item
                name="SP"
                label="Selling Price"
                rules={[
                  {
                    required: true,
                    message: "Please input Selling Price of Product",
                  },
                ]}
              >
                <InputNumber />
              </Form.Item>
              <Form.Item
                name="Quantity"
                label="Quantity"
                rules={[
                  {
                    required: true,
                    message: "Please input Quantity of Product",
                  },
                ]}
              >
                <InputNumber />
              </Form.Item>
              <Form.Item
                name="upload"
                label="Upload Image"
                valuePropName="fileList"
                getValueFromEvent={normFile}
                rules={[
                  {
                    required: true,
                    message: "Please uplaod Image ",
                  },
                ]}
              >
                <Upload name="logo" listType="picture">
                  <Button icon={<UploadOutlined />}>Click to upload</Button>
                </Upload>
              </Form.Item>
              <Form.Item
                name="agreement"
                valuePropName="checked"
                rules={[
                  {
                    validator: (_, value) =>
                      value
                        ? Promise.resolve()
                        : Promise.reject(
                            new Error(
                              "Please read all above information and click on Checkbox"
                            )
                          ),
                  },
                ]}
                {...tailFormItemLayout}
              >
                <Checkbox>
                  I have read the <b>Information</b>
                </Checkbox>
              </Form.Item>
              <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                  Create
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Col>
        <Col span={24}>
          <Button type="primary" id="show" onClick={handlePreview}>
            Show data
          </Button>
        </Col>

        {/* to Display Catlog */}
        <Col span={24}>
          {isPreviewShown && (
            <Table dataSource={dataSource}>
              <Column title="ID" dataIndex="ID" key="ID" />
              <Column title="Name" dataIndex="Name" key="Name" />
              <Column
                title="Description"
                dataIndex="Description"
                key="Description"
              />
               <Column title="Cost" dataIndex="Cost" key="Cost" />
              <Column
                title="Selling Prize"
                dataIndex="SellingPrize"
                key="SellingPrize"
              />
              <Column
                title="Action"
                key="action"
                render={(text, record) => (
                  <Space size="middle">
                    <Button>Update </Button>
                    <Button onClick={filterdata}>Delete</Button>
                  </Space>
                )}
              />
            </Table>
          )}
        </Col>
      </Row>

      
    </div>
  );
};

export default CreateProduct;
